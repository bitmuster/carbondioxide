#!/usr/bin/env python3

import unittest
from unittest.mock import MagicMock, patch

from world import World, TreeModel
from world import World, BiomassModel
from constants import Constants as c

class TestWorldModel(unittest.TestCase):
    """Tests for the abstract world model"""

    def test_update_stats(self):
        tm = TreeModel(10, 10)
        bm = BiomassModel(10, 10)
        world = World(tm, bm)
        s = world.update_stats()
        ret = world.get_co2_atmospheric()
        self.assertEqual(ret, c.co2_g_atmosphere)
        self.assertTrue(s)

    def test_update_statc_end(self):
        tm = TreeModel(10, 10)
        bm = BiomassModel(10, 10)
        world = World(tm, bm)
        tm.plant_trees(int(3e11))
        s = world.update_stats()
        self.assertFalse(s)

    def test_step(self):
        tm = TreeModel(c.tree_maturity, c.tree_max_age)
        bm = BiomassModel(c.biomass_decay_duration,
                                          c.biomass_max_age)
        world = World(tm, bm)
        r = world.step()
        self.assertEqual(r, (0, 0, 0, c.co2_g_atmosphere, True))

class TestWorldModelBasic(TestWorldModel):
    def setUp(self):
        TestWorldModel.setUp(self)
        tm = TreeModel(10, 10)
        bm = BiomassModel(10, 10)
        self.world = World(tm,bm)

    def test_carpenters_add_get(self):
        self.world.add_carpenters(10)
        self.assertEqual(self.world.carpenters, 10)
        self.world.add_carpenters(100)
        self.assertEqual(self.world.carpenters, 110)

    def test_carpenters_build_basic(self):
        self.world.stock_large_trees = 1000
        self.world.add_carpenters(1)
        self.world.step()
        self.assertEqual(self.world.houses, 1)
        self.assertEqual(self.world.stock_large_trees, 900)
        self.assertEqual(self.world.cash, c.price_per_house)

    def test_carpenters_build_basic_3(self):
        self.world.stock_large_trees = 1000
        self.world.add_carpenters(3)
        self.world.step()
        self.assertEqual(self.world.houses, 3)
        self.assertEqual(self.world.stock_large_trees, 700)

    def test_carpenters_build_too_many(self):
        self.world.stock_large_trees = 1000
        self.world.add_carpenters(22)
        self.world.step()
        self.assertEqual(self.world.houses, 10)
        self.assertEqual(self.world.stock_large_trees, 0)
        self.assertEqual(self.world.cash, c.price_per_house * 10)

    def test_credit_and_cash(self):
        self.assertEqual(self.world.cash, 0)
        self.assertEqual(self.world.credit, 0)
        self.world.add_credit(1000)
        self.world.step()
        self.assertEqual(self.world.cash, 1000-1000*c.interest_rate_month)

    def test_credit_and_cash_b(self):
        self.assertEqual(self.world.cash, 0)
        self.assertEqual(self.world.credit, 0)
        self.world.add_credit(1000)
        self.assertEqual(self.world.cash, 1000)
        self.assertEqual(self.world.credit, -1000)
        self.world.step()
        self.assertEqual(self.world.cash, 1000-1000*c.interest_rate_month)
        self.world.remove_credit(500)
        self.assertAlmostEqual(self.world.cash, 500-1000*c.interest_rate_month)

    def test_credit_and_cash_c(self):
        self.world.add_credit(100)
        self.world.remove_credit(200)
        self.assertEqual(self.world.cash, 0)
        self.assertEqual(self.world.credit, 0)

    def test_credit_and_cash_d(self):
        self.world.add_credit(1)
        self.world.step()
        self.world.remove_credit(1)
        # leftove will be the part that we cannot pay back due to the
        # interest
        self.assertAlmostEqual(self.world.credit, -c.interest_rate_month,
                               places = 4)
        # let money appear
        self.world.cash = 1
        # pay everything back
        self.world.remove_credit(1)
        self.assertEqual(self.world.credit, 0)
        self.assertEqual(self.world.cash, 1 - c.interest_rate_month)

    def test_credit_remove_assert(self):
        self.world.credit = 1
        with self.assertRaises(AssertionError):
            self.world.remove_credit(1)

