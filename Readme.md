

# CarbonDioxide #

A trading simulation game.

You fund a company that has the target to rescue earth from global warming.
Shis shall done by planting trees and consuming carbondioxide.
However, your company acts as a kind of paperclip maximizer and
neglegts lots of other influences and factors. See yourself how far you
can go.

Could be a lot like Universal-Paperclips (http://www.decisionproblem.com/paperclips/).
But with maybe more realistic concepts.

CarbonDioxide is in the state of a techdemo. It is playable but lacks
important functionality.

## Prerequisites ##

- Python3
- Scipy & Numpy
- Matplotlib
- Gtk3

## ToDo ##

- UI: Display amount of stuff
- UI: Improve overview of numbers
- Add money -> sell goods made from wood
- Adjust goal, we do not want to consume all co2
- Add multiple types of trees / plants
- Concept for dimension (currently the company is a dot, i.e. no distance)
    - Maybe restrict that trees seed themselves to max of 1km**2
- Bio-Coal in the supply chain?:
    - https://de.wikipedia.org/wiki/Biokohle
    - https://www.umweltbundesamt.de/sites/default/files/medien/378/publikationen/texte_04_2016_chancen_und_risiken_des_einsatzes_von_biokohle.pdf
