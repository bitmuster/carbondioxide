#!/usr/bin/env python3

import unittest
from unittest.mock import MagicMock, patch

from world import BiomassModel
from constants import Constants as c

class TestBiomassModel(unittest.TestCase):
    """Tests for the generic tree model"""

    def test_pass(self):
        self.assertEqual(1, 1)

    def test_init(self):
        decay_time = 12*10
        max_age = 240
        bm = BiomassModel(decay_time, max_age)
        bm.add(5)

    def test_transform(self):
        bm =  BiomassModel(120, 200)
        self.assertEqual(bm.sum(), 0)
        bm.transform()
        bm.add(5)
        self.assertEqual(bm.sum(), 5)

    def test_get_model(self):
        bm =  BiomassModel(5, 6)
        m = bm.get_model()
        self.assertEqual(len(m), 6)
        self.assertEqual(m, [0, 0, 0, 0, 0, 0])
        bm.add(5)
        m = bm.get_model()
        self.assertEqual(m, [5, 0, 0, 0, 0, 0])
        bm.transform()
        m = bm.get_model()
        self.assertEqual(m, [0, 4, 0, 0, 0, 0])

    def test_out_of_scope(self):
        bm =  BiomassModel(3, 4)
        m = bm.get_model()
        bm.add(100)
        m = bm.get_model()
        self.assertEqual(bm.out_of_scope, 0)
        self.assertEqual(m, [100, 0, 0, 0])
        bm.transform()
        m = bm.get_model()
        self.assertEqual(m, [0, 67, 0, 0])
        bm.transform()
        m = bm.get_model()
        self.assertEqual(m, [0, 0, 45, 0])
        bm.transform()
        m = bm.get_model()
        self.assertEqual(m, [0, 0, 0, 30])
        bm.transform()
        self.assertEqual(bm.out_of_scope, 20)
        m = bm.get_model()
        self.assertEqual(m, [0, 0, 0, 0])

