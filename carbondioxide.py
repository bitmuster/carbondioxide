#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Carbondioxide main module"""

# https://python-gtk-3-tutorial.readthedocs.io/en/latest/builder.html
# https://developer.gnome.org/gtk3/stable/
# PyGlibClass Reference https://developer.gnome.org/pygobject/stable/glib-functions.html
# Old:
# PyGObject API Reference https://lazka.github.io/pgi-docs/
# PyGObject Reference Manual http://www.pygtk.org/docs/pygobject/glib-functions.html

#How to find docu
#================
#
# GtkObjects: Glade -> Right click -> Open Documentation : GTK+ 3 Reference Manual
#  Also on: http://developer.gnome.org/gtk3/.
#
# help(Gtk.Grid)



# Install this on debian:
# apt-get install python3-cairocffi python3-gi-cairo python3-matplotlib

import sys

import gi # GObject introspection
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib

from matplotlib.figure import Figure
import matplotlib.animation as animation
#import matplotlib.lines as lines

import numpy

from numpy import arange

# TODO this check does not work
try:
    from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as FigureCanvas
except ImportError:
    print('Please install matplotlib.backends.backend_gtk3agg')
    sys.exit(1)


from world import TreeModel, BiomassModel, World
from auto_player import AutoPlayer
from constants import Constants as c

AUTOPLAY = False

s = None
t = None

def break_to_debugger():
    import pdb
    pdb.set_trace()

class GuiHandler:
    def __init__(self, model, game):
        self.game = game
        self.model = model
        self.amount_entries = {}
        self.multiplicator = 1

        self.state_label = builder.get_object('state_label')
        self.update_world_state(' CRITICAL ','red')

    def update_world_state(self, state, color):
        self.state_label.set_text(state)
        c = Gdk.color_parse(color)
        self.state_label.modify_bg(Gtk.StateType.NORMAL, c)

    def plant_one_tree(self, button):
        #print('Button plant_one_tree')
        None

    def stop(self, button):
        #print('Button stop')
        None

    def play(self, button):
        #print('Button play')
        self.game.set_timer(1000)

    def fast(self, button):
        #print('Button fast')
        self.game.set_timer(100)

    def fastforward(self, button):
        #print('Button fastforward')
        self.game.set_timer(20)

    def ff_forward(self, button):
        #print('Button fastfastforward')
        self.game.set_timer(5)

    def add_land(self, button):
        #print('Button add_land')
        self.model.buy_land(self.multiplicator)
        entry = self.amount_entries["Land"]
        entry.set_text( str(self.model.world.get_available_km2_forest()))

    def remove_land(self, button):
        #print('Button remove_land')
        self.model.buy_land(self.multiplicator)

    def add_tree(self, button):
        #print('Button add_tree')
        None

    def remove_tree(self, button):
        #print('Button remove_tree')
        None

    def add_forester(self, button):
        #print('Button add_forester')
        self.model.hire_forester(self.multiplicator)
        entry = self.amount_entries["Forester"]
        entry.set_text( str(self.model.world.get_foresters()))

    def remove_forester(self, button):
        #print('Button remove_forester')
        None

    def add_lumberjack(self, button):
        #print('Button add_lumberjack')
        self.model.hire_lumberjack(self.multiplicator)
        entry = self.amount_entries["Lumberjack"]
        entry.set_text( str(self.model.world.get_lumberjacks()))

    def remove_lumberjack(self, button):
        #print('Button remove_lumberjack')
        None

    def add_carpenter(self, button):
        self.model.hire_carpenter(self.multiplicator)
        entry = self.amount_entries["Carpenter"]
        entry.set_text( str(self.model.world.carpenters))

    def remove_carpenter(self, button):
        None

    def add_credit(self, button):
        self.model.world.add_credit(self.multiplicator)
        entry = self.amount_entries["Credit"]
        entry.set_text( str(self.model.world.credit))

    def remove_credit(self, button):
        self.model.world.remove_credit(self.multiplicator)
        entry = self.amount_entries["Credit"]
        entry.set_text( str(self.model.world.credit))

    def multiplicator_changed_cb(self, entity):
        #print('Changed multiplicator ' + entity.get_active_text())
        self.multiplicator = int(entity.get_active_text())

    def add_amount_entry(self, name, entry):
        self.amount_entries[name]=entry

    def menu_quit(self, *args):
        Gtk.main_quit(*args)
        self.model.quit()

    def window_destroy(self, *args):
        Gtk.main_quit(*args)
        self.model.quit()


class DataModel():

    def __init__(self, text_buffer):
        self.text_buffer = text_buffer
        self.month = 0
        self.sleep_time = 1
        self.active = True

        self.tree_model = TreeModel(c.tree_maturity, c.tree_max_age)
        self.biomass_model = BiomassModel(c.biomass_decay_duration,
                                          c.biomass_max_age)
        self.world = World(self.tree_model, self.biomass_model)
        self.gui_handler = None

    def quit(self):
        self.active = False

    def set_sleep_time(self, sleeptime):
        self.sleep_time = sleeptime

    def step(self):

        land_usage_ratio, new_trees, harvested, rest_co2, state = self.world.step()
        harvested_overall = self.tree_model.get_harvested_overall()
        assert len(self.tree_model.get_model()) == c.tree_max_age

        text1 = ' Year: {}; Month: {};'.format( int(self.month/12), self.month)
        text1 += ' Cash {:.2f}€;'.format(self.world.cash)
        text1 += ' Credit {}€;'.format(self.world.credit)
        text1 += ' Houses {};'.format(self.world.houses)
        text1 += ' Stock Trees {};'.format(self.world.stock_large_trees)
        text1 += ' Trees owned: {:d}; Ratio land usage: {:.2e}%;\n'\
            .format( int(self.tree_model.sum()), land_usage_ratio*100)

        text2 = ' New trees per month: %e; Trees chopped per month: %e;\n'%\
            (new_trees, harvested)

        text3 = ' Atmosphereic CO2: {0:d}g; Performance {1:.6f}%;' \
            .format(int(rest_co2), 100-100 * rest_co2/c.co2_g_atmosphere) \
            + ' Overall harvested trees {0:d}; Biomass {1:d}g;\n'\
            .format(int(harvested_overall), self.biomass_model.sum())

        text4 = ' Debug Infos: Dead trees (out of scope): {};' \
            .format(self.tree_model.get_dead_out_of_scope())

        # Solution:
        self.target = c.co2_g_atmosphere / c.co2_g_per_large_tree
        text4 += " Current target is e.g. have {:d} / {:.2e} large trees"\
            .format(int(self.target), float(self.target))

        text = "\n" + text1 + text2 + text3 + text4 +"\n"

        self.text_buffer.set_text(text)
        self.month += 1

        if not state:
            #print("You rescued earth in %i months"%self.month)
            self.gui_handler.update_world_state(" CLEAN ", "green")
        # To keep the timer alive
        return self.active

    def buy_land(self, amount=1):
        self.world.add_available_km2_forest(amount)
        print('You own ' + str(self.world.get_available_km2_forest()) \
            + ' square kilometers of forest. This is {0:.2f}%'\
            .format(100*self.world.get_available_km2_forest()/c.earth_land_surface) \
            + ' of the earth surface')
        new_trees = int(c.max_trees_per_km2_forest * amount\
             * c.tree_density_on_bought_land)
        print('You also bought %i trees'%new_trees)

        got = 0
        # asume it is a triangle
        # half of max lifespan for bought trees
        Y0 = new_trees / c.bought_tree_max_age *2
        for month in range(int(c.bought_tree_max_age)):
            new_in_month = int(Y0 - Y0 / c.bought_tree_max_age * month)
            self.tree_model.set_month(month, self.tree_model.get_month(month) \
                + new_in_month)
            got += new_in_month
        print('You also bought %i tree %i planted'%(new_trees, got))

    def hire_lumberjack(self, amount=1):
        self.world.add_lumberjacks(amount)
        #print('You employ ' + str(self.world.get_lumberjacks()) + ' lumberjacks')

    def hire_forester(self, amount=1):
        self.world.add_foresters(amount)
        #print('You employ ' + str(self.world.get_foresters()) + ' foresters')

    def hire_carpenter(self, amount = 1):
        self.world.add_carpenters(amount)

    def get_sum(self):
        return self.tree_model.sum()

    def set_gui_handler(self, handler):
        self.gui_handler = handler

class Game:

    def __init__(self):
        self.timer = None
        self. timeout = 1000
        self.timer = GLib.timeout_add(self.timeout, data_model.step)

    def set_timer(self, timeout):
        self.timeout = timeout
        GLib.source_remove(self.timer)
        self.timer = GLib.timeout_add(self.timeout, data_model.step)

def add_gui_inputs(parent_grid, position, name, plus_handler, minus_handler, \
                gui_handler ):
    namelabel = Gtk.Label()
    j = namelabel.get_justify()
    a = namelabel.get_alignment()
    namelabel.set_alignment(0, 0.5)
    #c = Gdk.color_parse("red")
    #namelabel.modify_bg(Gtk.StateType.NORMAL, c)
    amountentry = Gtk.Entry()
    infoview = Gtk.TextView()
    text_buffer = infoview.get_buffer()
    namelabel.set_text(name)
    amountentry.set_text('000 000')
    text_buffer.set_text('Infos \n more stuff and so on \n Stuff')
    plusbutton = Gtk.Button()
    plusbutton.set_label('+')
    plusbutton.connect_data('clicked', plus_handler)
    minusbutton = Gtk.Button()
    minusbutton.set_label('-')
    minusbutton.connect_data('clicked', minus_handler)
    grid = Gtk.Grid()

    grid.attach(namelabel, 1, 1, 3, 1) # left top with height
    grid.attach(amountentry, 3, 2, 1, 1)
    grid.attach(plusbutton, 1, 2, 1, 1)
    grid.attach(minusbutton, 2, 2, 1, 1)
    grid.attach(infoview, 4, 1, 1, 2)
    frame = Gtk.Frame()
    frame.set_label('A frame non working shadow ')
    frame.set_border_width(10)
    frame.set_shadow_type( Gtk.ShadowType(Gtk.ShadowType.ETCHED_OUT))
    frame.add(grid)
    parent_grid.attach(frame, 1, position, 1, 1)
    gui_handler.add_amount_entry(name, amountentry)


if __name__ == "__main__":


    glade = 'gui.glade'
    builder = Gtk.Builder()
    builder.add_from_file(glade)
    main_window = builder.get_object('main_window')
    text_view = builder.get_object('text_view')
    multiplicator = builder.get_object('multiplicator')
    input_viewport = builder.get_object('input_viewport')
    resource_viewport = builder.get_object('ressource_viewport')

    print(type(multiplicator))

    multiplicator.insert_text(0, '1')
    multiplicator.insert_text(1, '10')
    multiplicator.insert_text(2, '100')
    multiplicator.insert_text(3, '1000')
    multiplicator.insert_text(4, '10000')
    multiplicator.insert_text(5, '100000')
    multiplicator.insert_text(6, '1000000')
    multiplicator.set_active(0)

    input_grid = Gtk.Grid()
    input_viewport.add(input_grid)

    text_buffer = text_view.get_buffer()

    data_model = DataModel(text_buffer)
    mygame = Game()
    gui_handler = GuiHandler(data_model, mygame)
    player = AutoPlayer(gui_handler)

    data_model.set_gui_handler(gui_handler)

    add_gui_inputs(input_grid, 0, 'Tree',
                   gui_handler.add_tree, gui_handler.remove_tree, gui_handler)
    add_gui_inputs(input_grid, 1, 'Land',
                   gui_handler.add_land, gui_handler.remove_land, gui_handler)
    add_gui_inputs(input_grid, 2, 'Forester',
                   gui_handler.add_forester, gui_handler.remove_forester, gui_handler)
    add_gui_inputs(input_grid, 3, 'Lumberjack',
                   gui_handler.add_lumberjack,
                   gui_handler.remove_lumberjack, gui_handler)
    add_gui_inputs(input_grid, 4, 'Carpenter',
                   gui_handler.add_carpenter,
                   gui_handler.remove_carpenter, gui_handler)
    add_gui_inputs(input_grid, 5, 'Credit',
                   gui_handler.add_credit,
                   gui_handler.remove_credit, gui_handler)

    # Subplots within a larger figure
    afigure = Figure(figsize=(5, 4), dpi=100)
    acanvas = FigureCanvas(afigure) # a Gtk.DrawingArea
    acanvas.set_size_request(800, 300*3)

    axes = afigure.add_subplot(311, ylabel='Trees / Age')#, xlabel='Age')
    axes_workers= afigure.add_subplot(312, ylabel='Workers / Months')#, xlabel='Months')
    axes_cut= afigure.add_subplot(313, ylabel='Harvested Trees / Months')#, xlabel='Months')

    # Separate Plots
    biomassfigure = Figure(figsize=(5, 4), dpi=100)
    canvas_biomass = FigureCanvas(biomassfigure) # a Gtk.DrawingArea
    canvas_biomass.set_size_request(600, 300)
    axes_bio = biomassfigure.add_subplot(111, ylabel='Biomass / Age')#, xlabel='Age')


    # Press that into a grid
    resource_grid = Gtk.Grid()
    resource_viewport.add(resource_grid)
    resource_grid.attach(acanvas, 1, 1, 1, 1)
    resource_grid.attach(canvas_biomass, 1, 2, 1, 1)

    # Configure Axes

    # Trees
    tree_max_age = 200*12

    axes.set_xlim(0, tree_max_age)
    axes.set_ylim(0, 500)
    axes.plot([c.tree_maturity, c.tree_maturity], [0, 1e9], "r-")
    axes.plot([c.tree_harvest_begin, c.tree_harvest_begin], [0, 1e9],
              ":", color='gray')
    axes.plot([c.tree_harvest_end, c.tree_harvest_end], [0, 1e9],
              ":", color='gray')

    t = arange(tree_max_age)
    s = [42] * tree_max_age
    myplot, = axes.plot(t, s) # TODO: A comma why???

    #
    axes_bio.set_xlim(0, c.biomass_max_age)
    biomass_max_age = 200*12

    t2 = arange(biomass_max_age)
    s2 = [0] * biomass_max_age


    myplot_workers, = axes_workers.plot([0], [0]) # TODO: A comma why???
    axes_workers_2 = axes_workers.twinx()
    myplot_workers_2, = axes_workers_2.plot([0], [0]) # TODO: A comma why???

    myplot_cut, = axes_cut.plot([0], [0]) # TODO: A comma why???

    myplot_bio, = axes_bio.plot(t2, s2) # TODO: A comma why???
    axes_bio.plot([88, 88], [0, 1e9], "r-")

    lumberjack_history = [0]
    forester_history = [0]
    cut_history = [0]

    def anifunc(val):
        trees = data_model.tree_model.get_model()
        axes.set_ylim(0, max(trees) * 1.1)
        myplot.set_ydata(trees),

        lumberjack_history.append( data_model.world.get_lumberjacks())
        forester_history.append( data_model.world.get_foresters())
        cut_history.append( data_model.world.tree_model.get_harvested_overall())

        s2 = forester_history
        t2 = arange(len(forester_history))
        s3 = lumberjack_history
        t3 = arange(len(lumberjack_history))
        s4 = cut_history
        t4 = arange(len(cut_history))

        axes_workers.set_ylim(0, max(s2)*1.1)
        axes_workers.set_xlim(0, max(t2))
        myplot_workers.set_ydata(s2)
        myplot_workers.set_xdata(t2)

        axes_workers_2.set_ylim(0, max(s3)*1.1)
        #axes_workers_2.set_xlim(0, max(t3))
        myplot_workers_2.set_ydata(s3)
        myplot_workers_2.set_xdata(t3)

        axes_cut.set_ylim(0, max(s4)*1.1)
        axes_cut.set_xlim(0, max(t4))
        myplot_cut.set_ydata(s4)
        myplot_cut.set_xdata(t4)
        return False


    def anifunc2(val):
        biomass = data_model.biomass_model.get_model()
        s2 = biomass
        t2 = arange(c.biomass_max_age)
        axes_bio.set_ylim(min(s2)*1.1, max(s2) * 1.1)
        myplot_bio.set_ydata(s2)
        myplot_bio.set_xdata(t2)
        return False

    ani_interval = 400
    ani = animation.FuncAnimation(afigure, anifunc, interval=ani_interval)
    ani2 = animation.FuncAnimation(biomassfigure, anifunc2, interval=ani_interval)

    builder.connect_signals(gui_handler)
    main_window.show_all()

    if AUTOPLAY:
        player.start()
    Gtk.main()
