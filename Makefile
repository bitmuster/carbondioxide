
test:
	python3 -m unittest

lint:
	pylint3 *.py --no-docstring-rgx=test_*

coverage:
	python3-coverage run -m unittest
	python3-coverage report
	python3-coverage html

pyreverse:
	pyreverse3 carbondioxide.py world.py constants.py
	dot -Tpng classes_No_Name.dot > png.png
