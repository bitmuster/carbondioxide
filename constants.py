"""Simulation constants"""

class Constants:
    """Storage for our world constants"""
    tree_death_rate = 0.01 # per year
    tree_maturity = 120
    tree_reproduction_rate = 12 * 4 # one every ... months
    max_trees_per_km2_forest = (1000/5)**2 # A tree every 5m
    trees_per_forester = 20 * 20 # per month
    trees_per_lumberjack = 10 * 20 # per month
    tree_max_age = 200 *12 # in months
    bought_tree_max_age = 1000
    tree_density_on_bought_land = 0 # 0: No bought trees, otherwise e.g. 0.3
    tree_harvest_begin = 30*12
    tree_harvest_end = 60*12

    tree_decay_duration = 10 *12
    biomass_decay_duration = 1 * 12
    biomass_max_age = 20

    # https://de.wikipedia.org/wiki/Erde
    earth_available_km2 = 510e6
    earth_available_sea_km2 = 360.570e3
    earth_available_land_km2 = 149.430e3
    earth_land_surface = 149430000 # https://de.wikipedia.org/wiki/Erde

    # https://de.wikipedia.org/wiki/Kohlenstoffzyklus
    #(2017) Amount of co2 in the Atmosphere
    co2_g_atmosphere = 850 * 1e9 * 1e6
    # TODO: However, we don't need to reduce all of that

    co2_mol_per_cubic_m = 406 #(2017)

    # 44 mol = 2*16 + 12
    c_per_co2 = 3 / 11 # = 0.2727...

    # https://de.wikipedia.org/wiki/Baum
    # 80 year old European beech; 15 m^3 wood; wheight 12.000 kg ; 6000Kg C
    g_per_large_tree = 12*1000
    c_g_per_large_tree = 6000 * 1e3
    co2_g_per_large_tree = c_g_per_large_tree / c_per_co2

    large_trees_per_house = 100 # lets assume this
    price_per_house = 50*1000
    interest_rate_month = 0.10 / 12 # 10% per year
