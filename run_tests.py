#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Main test entry"""

import unittest

from test_data_model import TestDataModel
from test_tree_model import TestTreeModel
from test_world import TestWorldModel
from test_biomass_model import TestBiomassModel

if __name__ == '__main__':
    unittest.main()
