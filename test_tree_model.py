#!/usr/bin/env python3

import unittest
from unittest.mock import MagicMock, patch

from world import TreeModel
from constants import Constants as c

class TestTreeModel(unittest.TestCase):
    """Tests for the generic tree model"""

    def test_pass(self):
        self.assertEqual(1, 1)

    def test_init(self):
        tree_model = TreeModel(maturity=10, max_age=100)
        mysum = tree_model.sum()
        self.assertEqual(mysum, 0)

    def test_plant_trees(self):
        tree_model = TreeModel(maturity=10, max_age=100)
        tree_model.plant_trees(1)
        mysum = tree_model.sum()
        self.assertEqual(mysum, 1)

    def test_get_tree_sum(self):
        tree_model = TreeModel(maturity=10, max_age=100)
        tree_model.plant_trees(1)
        mysum = tree_model.sum()
        self.assertEqual(mysum, 1)

    def test_get_tree_sum_n(self):
        tree_model = TreeModel(maturity=10, max_age=100)
        tree_model.plant_trees(3)
        mysum = tree_model.sum()
        self.assertEqual(mysum, 3)

        tree_model.plant_trees(3)
        mysum = tree_model.sum()
        self.assertEqual(mysum, 6)

    def test_get_model(self):
        tree_model = TreeModel(maturity=10, max_age=3)
        m = tree_model.get_model()
        self.assertEqual(m, [0, 0, 0])
        tree_model.plant_trees(3)
        m = tree_model.get_model()
        self.assertEqual(m, [3, 0, 0])

    def test_transform(self):
        tree_model = TreeModel(maturity=10, max_age=3)
        tree_model.plant_trees(1)
        self.assertEqual(tree_model.sum(0, 1), 1)
        tree_model.transform()
        self.assertEqual(tree_model.sum(1, 2), 1)
        tree_model.transform()
        self.assertEqual(tree_model.sum(2, 3), 1)
        self.assertEqual(tree_model.get_dead_out_of_scope(), 0)
        tree_model.transform()
        self.assertEqual(tree_model.sum(), 0)
        self.assertEqual(tree_model.get_dead_out_of_scope(), 1)


    def test_get_sum_on_range(self):
        tree_model = TreeModel(maturity=10, max_age=20)
        self.assertEqual(tree_model.sum(), 0)
        tree_model.plant_trees(1)
        self.assertEqual(tree_model.sum(), 1)
        self.assertEqual(tree_model.sum(0, 10), 1)
        self.assertEqual(tree_model.sum(1, 10), 0)
        tree_model.plant_trees(20)
        self.assertEqual(tree_model.sum(0, 10), 21)
        self.assertEqual(tree_model.sum(1, 10), 0)
        tree_model.transform()
        self.assertEqual(tree_model.sum(), 21)
        self.assertEqual(tree_model.sum(0, 0), 0)
        self.assertEqual(tree_model.sum(1, 2), 21)

    def test_calculate_new_trees(self):
        tm = TreeModel(maturity=10, max_age=20)
        n = tm.calculate_new_trees(0, 10)
        self.assertEqual(n, 0)
        n = tm.calculate_new_trees(1, 10)
        self.assertEqual(n, 400)

    @patch("random.randint")
    def test_process_age_and_disease(self, mock):
        mock.return_value = 100
        tm = TreeModel(maturity=10, max_age=20)

        dead = tm.process_age_and_disease()
        self.assertEqual(dead, 0)
        self.assertEqual(tm.sum(), 0)

        tm.plant_trees(400)
        dead = tm.process_age_and_disease()
        self.assertEqual(tm.sum(), 399)
        self.assertEqual(dead, 1)

    def test_tree_harvest(self):
        tm = TreeModel(maturity=10, max_age=c.tree_harvest_end*2)
        ret = tm.harvest(1)
        self.assertEqual(ret, 0)
        for i in range(c.tree_harvest_begin, c.tree_harvest_end):
            tm.set_month(i, 1)
        ret = tm.harvest(1)
        self.assertEqual(ret, c.trees_per_lumberjack)
        ret = tm.harvest(1)
        self.assertEqual(ret, c.tree_harvest_end \
                - c.tree_harvest_begin - c.trees_per_lumberjack)

    def test_land_usage_ratio(self):
        tm = TreeModel(maturity=10, max_age=20)
        ret = tm.land_usage_ratio(10)
        self.assertEqual(ret, 0)
        tm.plant_trees(20000*10)
        ret = tm.land_usage_ratio(10)
        self.assertEqual(ret, 0.5)

    @patch("random.randint")
    def test_lumberjack(self, mock):
        mock.return_value = 100
        jack = 10
        tm = TreeModel(c.tree_maturity, c.tree_max_age)
        self.assertEqual(tm.sum(), 0)
        tm.harvest(jack)
        tm.plant_trees(10000)
        self.assertEqual(tm.sum(), 10000)
        self.assertEqual(tm.get_harvested_overall(), 0)
        for i in range(c.tree_harvest_begin-1):
            tm.transform()
            h = tm.harvest(jack)
            self.assertEqual(h, 0, i)
            self.assertEqual(tm.get_harvested_overall(), 0, "Info: "+str(i))
        tm.transform()
        h = tm.harvest(jack)
        exp = c.trees_per_lumberjack * jack
        self.assertEqual(h, exp)
        self.assertEqual(tm.get_harvested_overall(), exp) # current hardcoded value
        #dm.step()
        #self.assertEqual(dm.jacked_overall, exp*2) # current hardcoded value
        #dm.step()
        #self.assertEqual(dm.jacked_overall, exp*3) # current hardcoded value

