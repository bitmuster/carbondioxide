"""Integration tests for the complete data model"""

import unittest
from unittest.mock import MagicMock, patch

from carbondioxide import DataModel
from constants import Constants as c

class TestDataModel(unittest.TestCase):

    def test_init(self):
        dm = DataModel(None)
        self.assertEqual(dm.get_sum(), 0)

    @patch("random.randint")
    def test_step123(self, mock):
        mock.return_value = 100
        dm = DataModel(MagicMock())
        self.assertEqual(dm.get_sum(), 0)
        dm.buy_land(10) # We started with 10 land by default now we need to add
        dm.step()
        self.assertEqual(dm.get_sum(), 0)
        dm.buy_land(1)
        dm.hire_forester(1)
        dm.step()
        # One tree died
        self.assertEqual(dm.get_sum(), c.trees_per_forester - 1)
        dm.step()
        # 399 new and 3 died
        self.assertEqual(dm.get_sum(), c.trees_per_forester *2 - 2 -2)

        for i in range(10):
            dm.step()
        self.assertEqual(dm.get_sum(), 4693) # current hardcoded value

        for i in range(100):
            dm.step()
        self.assertEqual(dm.get_sum(), 36439) # current hardcoded value
        self.assertEqual(dm.tree_model.dead_out_of_scope, 0)

    @patch("random.randint")
    def test_scope_loss(self, mock):
        mock.return_value = 100
        dm = DataModel(MagicMock())
        dm.buy_land(10) # We started with 10 land by default now we need to add
        self.assertEqual(dm.get_sum(), 0)
        dm.step()
        self.assertEqual(dm.get_sum(), 0)
        dm.buy_land(1000)
        dm.hire_forester(1000)

        self.assertEqual(dm.tree_model.dead_out_of_scope, 0)
        for i in range(c.tree_max_age):
            dm.step()
        self.assertEqual(dm.tree_model.dead_out_of_scope, 0)

        dm.step()
        #current measurement
        self.assertEqual(dm.tree_model.dead_out_of_scope, 53571)

        dm.step()
        #current measurement
        self.assertEqual(dm.tree_model.dead_out_of_scope, 106606)

