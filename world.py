"""World abstraction"""

from constants import Constants as c
import random

class ControlUi:
    """Input """
    def __init__(self):
        pass

class RssourceUi:
    """Output"""
    def __init__(self):
        pass

class World:
    """Our world abstraction"""

    def __init__(self, tree_model, biomass_model):
        """Constructor"""

        self.tree_model = tree_model
        self.biomass_model = biomass_model

        #rescources
        self.tree_model_slow = None
        self.tree_model_fast = None
        self.money = None
        self.co2_atmospheric = 0 # The rest of co2 in the atmosphere
        self.co2 = None
        self.biomass = None
        self.biomass_dacaying = None
        self.carbon = None
        self.houses = None
        self.feul = None
        self.energy = None #W/h
        self.gas = None
        self.wood = None
        self.available_km2_forest = 0
        self.stock_large_trees = 0
        self.houses = 0

        # Inputs all of these are integers
        self.foresters = 0 # Plant trees
        self.lumberjacks = 0 # Chop trees
        self._carpenters = 0 # Build houses from wood
        self.etstate = None # Base to plant trees on
        self.windpowerplant = None # Convert wind to power
        self.solarpowerplant = None # Convert solar energy to power
        self.solartofeul = None # Use algae to transform solar enery to feul
        self.combustionpowerplant = None # Burn biomass, wood, carbon, gas or feul to get power
        self.powertofeulplant = None # Transform power to feul
        self.powertogasplant = None # Transform power to gas
        self.co2trap = None # Catch atmospheric co2
        self._credit = 0 # Money borrowed from bank (always < 0)
        self._cash = 0

    def step(self):
        """Process a month"""

        new_trees = self.tree_model.calculate_new_trees(self.get_foresters(),
            self.get_available_km2_forest())
        self.tree_model.transform()
        self.tree_model.plant_trees(new_trees)

        dead_scope_loss = self.tree_model.get_dead_out_of_scope()
        dead = self.tree_model.process_age_and_disease()
        self.biomass_model.transform()
        self.biomass_model.add(dead * c.g_per_large_tree)

        harvested = self.tree_model.harvest(self.get_lumberjacks())
        state = self.update_stats()

        rest_co2 = self.get_co2_atmospheric()
        land_usage_ratio = self.tree_model.land_usage_ratio(
            self.get_available_km2_forest())

        self.stock_large_trees += harvested
        # only one house per month per carpenter
        if self.stock_large_trees >= c.large_trees_per_house:
            new_houses_max = self.stock_large_trees // c.large_trees_per_house
            new_houses = min(new_houses_max, self.carpenters)
            self.stock_large_trees -= c.large_trees_per_house \
                * new_houses
            self.houses += new_houses

            # We assume that we sell it immedetealy
            self.cash += new_houses * c.price_per_house

        self.cash += self.credit * c.interest_rate_month

        return land_usage_ratio, new_trees, harvested, rest_co2, state

    def register(self):
        """Register ressource usage"""
        pass

    def check_world(self):
        """The amount of c has to be constant"""
        pass

    def update_stats(self):
        """Update wold data fields"""
        # Assumed that we store the C from every tree, utopia
        # Yeah, transform the C into a black solid material and bury it "again"
        self.co2_atmospheric = c.co2_g_atmosphere - c.co2_g_per_large_tree \
            * (self.tree_model.sum() + self.tree_model.get_harvested_overall())
        # TODO: that rule is a bit harsh we should stop somewhere where
        #    our plants do not die of too little co2
        #    probalbly the value where earth has been before mankind
        # TODO: New goal start as fast as possible and stop at
        #    a healthy concentration
        if self.co2_atmospheric <= 0:
            #print("You rescued earth.")
            # Maybe with 3.8e+10 Trees at 1.02e+6 square kilometers;
            # E.g. 0.74% of the surface
            return False
        else:
            return True

    def get_co2_atmospheric(self):
        return self.co2_atmospheric

    def get_foresters(self):
        return self.foresters

    def get_lumberjacks(self):
        return self.lumberjacks

    def get_available_km2_forest(self):
        return self.available_km2_forest

    def add_available_km2_forest(self, amount):
        self.available_km2_forest += amount

    def add_foresters(self, amount):
        self.foresters += amount

    def add_lumberjacks(self, amount):
        self.lumberjacks += amount

    @property
    def carpenters(self):
        return self._carpenters

    def add_carpenters(self, amount):
        self._carpenters += amount

    @property
    def cash(self):
        """Cash available"""
        return self._cash

    @cash.setter
    def cash(self, value):
        self._cash = value

    @property
    def credit(self):
        return self._credit

    @credit.setter
    def credit(self, value):
        self._credit = value

    def add_credit(self, value):
        self._credit -= value
        self._cash += value

    def remove_credit(self, value):
        assert(self.credit < 0)
        if self._cash > value:
            if abs(self.credit) > value:
                self._credit += value
                self._cash -= value
            else:
                self._cash += self.credit
                self._credit = 0
        else: # less cash than value
            if abs(self.credit) > self._cash:
                self._credit += self._cash
                self._cash = 0
            else:
                self._cash += self._credit
                self._credit = 0

class Ressource:
    """Represent ressources"""

    def request(self):
        """Request ressource"""
        pass

    def transformation(self):
        """Transform ressource for one month"""
        pass

    def feedback(self):
        """Feedback transformation result"""
        pass

    def register(self):
        """Register at world model"""
        pass

class TreeModel(Ressource):
    """Represent the model of a generic tree"""

    def __init__(self, maturity, max_age):
        self.max_age = max_age
        self.trees = [0] * max_age
        self.maturity = maturity
        self.harvested_overall = 0
        self.dead_out_of_scope = 0

    def transform(self):
        """Transform the resource"""
        self.dead_out_of_scope += self.trees[-1]
        self.trees = [0] + self.trees
        del self.trees[-1]

    def get_dead_out_of_scope(self):
        """See how many trees got lost in our data model"""
        return self.dead_out_of_scope

    def plant_trees(self, amount):
        """Plant some trees"""
        self.trees[0] += amount

    def sum(self, min=0, max=None):
        """Return sum from min...max-1"""
        if max is None:
            max = self.max_age # we cannot access self before the function starts
        t = sum(self.trees[min:max])
        return sum(self.trees[min:max])

    def get_model(self):
        """temporary"""
        return self.trees[:]

    def get_month(self, m):
        """temporary"""
        return self.trees[m]

    def set_month(self, m, i):
        """temporary"""
        self.trees[m] = i

    def calculate_new_trees(self, foresters, available_km2_forest):
        """Check how much new trees will be sowed"""
        current_trees = self.sum()
        max_trees = int(available_km2_forest * c.max_trees_per_km2_forest)
        space_left = (1-self.land_usage_ratio(available_km2_forest))

        new_trees = int(self.sum(c.tree_maturity, self.max_age)\
                     / c.tree_reproduction_rate * space_left)
        new_trees += int(c.trees_per_forester * foresters * space_left)

        if current_trees >= max_trees:
            new_trees = 0
        elif (current_trees + new_trees) >= (max_trees):
            new_trees = max_trees - current_trees

        return new_trees

    def process_age_and_disease(self):
        """See how many die of deseases"""
        # Implement death rate
        dead = 0
        for i in range(self.max_age):
            if self.get_month(i) == 0:
                pass
            # TODO How to deal with small tree amounts
            #      Can we use a nice probabilictic model everywhere?
            elif self.get_month(i) <= 100:
                if random.randint(1, 100) < self.get_month(i):
                    self.set_month(i, self.get_month(i) - 1)
            else:
                # Hint we cannot round here as trees stop to die when the value
                # is smaller than 0.5
                # We shoud use random distribution here
                survive = int(self.get_month(i) * (1 - (c.tree_death_rate / 12)))
                dead += self.get_month(i) - survive
                self.set_month(i, survive)
        return dead

    def harvest(self, lumberjacks):
        """Model tree harvest
        Here, we should also take the tree majurity into account
        Maybe also work with Kg of wood. Which is growing linearly with age
        """

        harvest = list(range(c.tree_harvest_begin, c.tree_harvest_end))
        jacked = 0
        to_jack = c.trees_per_lumberjack * lumberjacks
        for j in harvest:
            if to_jack == 0:
                break
            if self.get_month(j) >= to_jack:
                self.set_month(j, self.get_month(j) - to_jack)
                jacked += to_jack
                to_jack = 0
            else:
                jacked += self.get_month(j)
                to_jack = to_jack - self.get_month(j)
                self.set_month(j, 0)


        self.harvested_overall += jacked
        return jacked

    def get_harvested_overall(self):
        return self.harvested_overall

    def land_usage_ratio(self, available):
        """Derive how much of the land is already occupied"""
        current_trees = self.sum()
        max_trees = int(available * c.max_trees_per_km2_forest)
        if current_trees > 0:
            land_usage_ratio = current_trees / max_trees
        else:
            land_usage_ratio = 0
        return land_usage_ratio

class BiomassModel:

    def __init__(self, decay_duration, max_age):
        self.model = [0] * max_age # in g
        self.out_of_scope =0
        self.decay_duration = decay_duration

    def add(self, amount):
        self.model[0] += amount

    def sum(self):
        return sum(self.model)

    def transform(self):
        for i in range(len(self.model)):
            self.model[i] = round(self.model[i] * (1-1/self.decay_duration))
        self.out_of_scope += self.model[-1]
        self.model = [0] + self.model
        del self.model[-1]

    def get_model(self):
        return self.model
